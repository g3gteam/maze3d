#include "Maze.h"

Maze::Maze(uint32_t mazeSize) :
	mazeSize(mazeSize)
{
	allocate();
}

Maze::Maze(const Maze & other)
{
	*this = other;
}

Maze & Maze::operator=(const Maze & other)
{
	this->mazeSize = other.mazeSize;
	allocate();
	if (other.data)
	{
		for (unsigned rowIndex = 0; rowIndex < mazeSize; ++rowIndex)
			for (unsigned colIndex = 0; colIndex < mazeSize; ++colIndex)
				data[rowIndex][colIndex] = other.data[rowIndex][colIndex];
	}
	return *this;
}

Maze::Tile* Maze::operator[](int x)
{
	return data[x];
}

uint32_t Maze::getSize() const
{
	return mazeSize;
}

Maze::~Maze()
{
	deallocate();
}

bool Maze::isPositionAvailableForMove(const std::pair<int, int>& position) const
{
	return this->data[position.first][position.second] == Maze::Tile::None;
}

void Maze::allocate()
{
	if (data != nullptr) deallocate();
	data = new Tile*[mazeSize];
	for (unsigned index = 0; index < mazeSize; ++index)
		data[index] = new Tile[mazeSize];
}

void Maze::deallocate()
{
	if (data == nullptr) return;
	for (unsigned index = 0; index < mazeSize; ++index)
		if (data[index] != nullptr)
		{
			delete[] data[index];
			data[index] = nullptr;
		}
	data = nullptr;
	mazeSize = 0;
}
