#pragma once
#include <memory>
#include "Mesh.h"

class Model
{
public:
	Model(Shader* shader, std::shared_ptr<Mesh> mesh);
	void draw();

	std::shared_ptr<Mesh> mesh;
	glm::mat4 transform;
private:
	Shader* shader;
};

