#include "AudioSource.h"

AudioSource::AudioSource():
	position(0.f, 0.f, 0.f),
	velocity(0.f, 0.f, 0.f)

{
	alGenBuffers(1, &buffer);
	alGenSources(1, &id);
}

void AudioSource::SetSource()
{
	alDistanceModel(AL_EXPONENT_DISTANCE);
	alSourcef(id, AL_ROLLOFF_FACTOR, 1.0f);
	alSourcef(id, AL_REFERENCE_DISTANCE, 4.0f);
	alSourcef(id, AL_MAX_DISTANCE, 20.0f);

	alSourcei(id, AL_BUFFER, buffer);
	alSourcef(id, AL_PITCH, 1.0f);
	alSourcef(id, AL_GAIN, 1.0f);
	alSourcefv(id, AL_POSITION, glm::value_ptr(position));
	alSourcefv(id, AL_VELOCITY, glm::value_ptr(velocity));
	alSourcei(id, AL_LOOPING, AL_TRUE);

}

void AudioSource::SetPosition(glm::vec3 otherPosition)
{
	position.x = otherPosition.x;
	position.y = otherPosition.y;
	position.z = otherPosition.z;
	alSourcefv(id, AL_POSITION, glm::value_ptr(position));
}
