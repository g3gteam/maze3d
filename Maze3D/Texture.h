#pragma once
#include <string>

class Texture
{
public:
	Texture() = default;
	Texture(const std::string& filePath, const std::string& type = "");
	unsigned int getId() const;
	std::string type;
private:
	unsigned int id = 0;
};

