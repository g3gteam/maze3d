#version 330 core
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aNormal;
layout (location = 2) in vec2 aTexCoords;

out vec2 TexCoords;
out vec3 fragPosition;
out vec3 fragNormal;
out vec4 fragPositionLightSpace;

uniform mat4 transform;
uniform mat4 view;
uniform mat4 projection;
uniform mat4 lightSpaceMatrix;

void main()
{
    fragPosition = vec3(transform * vec4(aPos, 1.0));
    fragNormal = mat3(transpose(inverse(transform))) * aNormal; 
    TexCoords = aTexCoords;    
    fragPositionLightSpace = lightSpaceMatrix * vec4(fragPosition, 1.0);
    gl_Position = projection * view * transform * vec4(aPos, 1.0);
}