#include "Plane.h"

Plane::Plane() :
	Mesh(false)
{
	addVertex({ { 0.0f, -1.0f, 0.0f}, {0.0f, 0.0f, 1.0f}, {0.0f, 0.0f} });
	addVertex({ { 1.0f,  0.0f, 0.0f}, {0.0f, 0.0f, 1.0f}, {1.0f, 1.0f} });
	addVertex({ { 1.0f, -1.0f, 0.0f}, {0.0f, 0.0f, 1.0f}, {1.0f, 0.0f} });
	addVertex({ { 1.0f,  0.0f, 0.0f}, {0.0f, 0.0f, -1.0f}, {1.0f, 1.0f} });
	addVertex({ { 0.0f, -1.0f, 0.0f}, {0.0f, 0.0f, -1.0f}, {0.0f, 0.0f} });
	addVertex({ { 0.0f,  0.0f, 0.0f}, {0.0f, 0.0f, -1.0f}, {0.0f, 1.0f} });
}
