#include "Texture.h"
#include <GL/glew.h>
#include <iostream>

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

Texture::Texture(const std::string & filePath, const std::string& type) :
	type(type)
{
	stbi_set_flip_vertically_on_load(true);
	int width, height, channelsCount;
	GLubyte* data = stbi_load(filePath.c_str(), &width, &height, &channelsCount, 0);
	if (data)
	{
		GLenum format;
		if (channelsCount == 1)
			format = GL_RED;
		else if (channelsCount == 3)
			format = GL_RGB;
		else if (channelsCount == 4)
			format = GL_RGBA;
		else
		{
			stbi_image_free(data);
			return;
		}

		glGenTextures(1, &id);
		glBindTexture(GL_TEXTURE_2D, id);
		glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, data);
		glGenerateMipmap(GL_TEXTURE_2D);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		stbi_image_free(data);
	}
	else 
	{
		std::cerr << "Failed to load texture " << filePath << "." << std::endl;
	}
}

unsigned int Texture::getId() const
{
	return id;
}