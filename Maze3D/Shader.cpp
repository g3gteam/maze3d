#include "Shader.h"
#include "Utils.h"
#include <fstream>
#include <iostream>
#include <glm/gtc/type_ptr.hpp>

Shader::Shader(const std::string & vertexPath, const std::string & fragmentPath)
{
	std::string vertexShaderString, fragmentShaderString;
	unsigned int vertexShaderId, fragmentShaderId;

	readShader(vertexPath, vertexShaderString);
	readShader(fragmentPath, fragmentShaderString);

	vertexShaderId = glCreateShader(GL_VERTEX_SHADER);
	fragmentShaderId = glCreateShader(GL_FRAGMENT_SHADER);

	compileShader(vertexShaderId, vertexShaderString.c_str());
	compileShader(fragmentShaderId, fragmentShaderString.c_str());

	linkShaders(vertexShaderId, fragmentShaderId);
}

Shader::~Shader()
{
	glDeleteProgram(id);
}

void Shader::use()
{
	glUseProgram(id);
}

void Shader::setBool(const std::string & name, bool value) const
{
	glUniform1i(glGetUniformLocation(id, name.c_str()), static_cast<int>(value));
}

void Shader::setFloat(const std::string & name, float value) const
{
	glUniform1f(glGetUniformLocation(id, name.c_str()), value);
}

void Shader::setInt(const std::string & name, int value) const
{
	glUniform1i(glGetUniformLocation(id, name.c_str()), value);
}

unsigned int Shader::getId() const
{
	return id;
}

bool Shader::readShader(const std::string & shaderPath, std::string& shaderString)
{
	std::ifstream shaderFile;
	shaderFile.exceptions(std::ios::failbit | std::ios::badbit);
	try
	{
		shaderFile.open(shaderPath);
		shaderString = std::move(Utils::readAll(shaderFile));
	}
	catch (const std::ios_base::failure& exception)
	{
		std::cerr << "Error: Shader file could not be read.\n" << exception.what() << std::endl;
		return false;
	}
	return true;
}

bool Shader::linkShaders(unsigned int vertexShaderId, unsigned int fragmentShaderId)
{
	int success;
	char messageLog[1024];
	id = glCreateProgram();
	glAttachShader(id, vertexShaderId);
	glAttachShader(id, fragmentShaderId);
	glLinkProgram(id);
	glGetProgramiv(id, GL_LINK_STATUS, &success);
	if (!success)
	{
		glGetProgramInfoLog(id, sizeof(messageLog) / sizeof(messageLog[0]), NULL, messageLog);
		std::cout << "Error: Shaders could not be linked.\n" << messageLog << std::endl;
	};
	return success;
}

bool Shader::compileShader(unsigned int shaderId, const char* shaderCode)
{
	int success;
	char messageLog[1024];
	glShaderSource(shaderId, 1, &shaderCode, NULL);
	glCompileShader(shaderId);
	glGetShaderiv(shaderId, GL_COMPILE_STATUS, &success);
	if (!success)
	{
		glGetShaderInfoLog(shaderId, sizeof(messageLog) / sizeof(messageLog[0]), NULL, messageLog);
		std::cout << "Error: Shader could not be compiled.\n" << messageLog << std::endl;
	};
	return success;
}

void Shader::setMat4(const std::string & name, const glm::mat4 &matrix) const
{
	glUniformMatrix4fv(glGetUniformLocation(id, name.c_str()), 1, GL_FALSE, glm::value_ptr(matrix));
}

void Shader::setVec3(const std::string & name, const glm::vec3 & vector) const
{
	glUniform3fv(glGetUniformLocation(id, name.c_str()), 1, glm::value_ptr(vector));
}
