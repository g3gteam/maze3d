#pragma once
#include <glm/glm.hpp>
#include <memory>
#include "Camera.h"

class Maze;
class Player
{
public:
	Player(const glm::vec3& position, std::shared_ptr<Camera> camera);

	enum class MovementDirection : uint8_t
	{
		NONE = 0,
		FORWARD,
		BACKWARDS,
		LEFT,
		RIGHT
	};
	void processMovement(MovementDirection movementDirection, float deltaTime, const Maze & maze, float blockSize);

	std::shared_ptr<Camera> camera;
	glm::vec3 position;
	float speedFactor = 2.5f;
};

