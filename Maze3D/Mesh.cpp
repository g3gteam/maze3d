#include "Mesh.h"

#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include "Shader.h"

Mesh::Mesh(bool usesEBO) :
	usesEBO(usesEBO)
{
	if(usesEBO)
		glGenBuffers(1, &indexBuffer);
	glGenBuffers(1, &vertexBuffer);
}

Mesh::~Mesh()
{
	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
	glDeleteBuffers(1, &vertexBuffer);
	if (usesEBO)
		glDeleteBuffers(1, &indexBuffer); 
}

void Mesh::addVertex(const Vertex& vertex)
{
	vertices.push_back(vertex);
}

void Mesh::addTriangleByIndices(const Triangle& triangle)
{
	if (!usesEBO)
		throw std::runtime_error("You cannot add indices if you are not using an EBO.");
	indices.push_back(triangle);
}

void Mesh::build()
{
	setBufferData(vertexBuffer, vertices.size(), vertices.data());
	if (usesEBO)
		setBufferData(indexBuffer, indices.size(), indices.data(), GL_ELEMENT_ARRAY_BUFFER);
	glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
	setVertexAttribPointer(0, vertices[0].position.length(), vertexBuffer);
	setVertexAttribPointer(1, vertices[0].normal.length(), vertexBuffer, offsetof(Vertex, normal));
	setVertexAttribPointer(2, vertices[0].textureCoordinates.length(), vertexBuffer, offsetof(Vertex, textureCoordinates));
}

void Mesh::update() {}

void Mesh::draw()
{
	if (usesEBO)
	{
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer);
		glDrawElements(GL_TRIANGLES, indices.size() * indices[0].size(), GL_UNSIGNED_INT, (void*)0);
	}
	else
	{
		glDrawArrays(GL_TRIANGLES, 0, vertices.size());
	}
}

void Mesh::setVertexAttribPointer(GLuint vertexAttribArrayID, GLuint componentCount, GLuint bufferID, GLuint offset)
{
	glEnableVertexAttribArray(vertexAttribArrayID);
	glVertexAttribPointer(vertexAttribArrayID, componentCount, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offset);
}