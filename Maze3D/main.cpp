#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/gtc/type_ptr.hpp>
#include <iostream>
#include <fstream>
#include <unordered_map>
#include "Shader.h"
#include "Cube.h"
#include "Model.h"
#include "ImportedModel.h"
#include "Player.h"
#include "MazeBuilder.h"
#include "Utils.h"
#include "Plane.h"
#include "Texture.h"
#include "AudioMaster.h"

#pragma comment(lib, "OpenGL32.lib")
#pragma comment(lib, "glew32.lib")
#pragma comment(lib, "glfw3.lib")
#pragma comment(lib, "OpenAL32.lib")
#pragma comment(lib, "assimp.lib")

constexpr int kScreenWidth = 1024;
constexpr int kScreenHeight = 768;
constexpr int kMazeHeight = 2;
constexpr int kShadowWidth = 1024;
constexpr int kShadowHeight = 1024;


glm::vec3 chestLocation(30.f, -0.5f, 30.f);

std::unordered_map<std::string, Texture> textures;

std::shared_ptr<Player> player;
float blockSize;
Maze maze;
bool gameWon = false;

AudioMaster audio;


namespace Callbacks
{
	void resize(GLFWwindow* window, int width, int height)
	{
		::player->camera->setViewport({ width, height });
	}
	void mouseMove(GLFWwindow* window, double x, double y)
	{
		::player->camera->processMouseMovement({ x, y });
	}
	void scroll(GLFWwindow* window, double x, double y)
	{
		::player->camera->processMouseScroll(y);
	}
}

void processInput(GLFWwindow* window, float deltaTime)
{
	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
		glfwSetWindowShouldClose(window, true);
	if (gameWon) return;
	if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS || glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS)
		player->processMovement(Player::MovementDirection::FORWARD, deltaTime, maze, blockSize);
	if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS || glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS)
		player->processMovement(Player::MovementDirection::BACKWARDS, deltaTime, maze, blockSize);
	if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS || glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS)
		player->processMovement(Player::MovementDirection::LEFT, deltaTime, maze, blockSize);
	if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS || glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS)
		player->processMovement(Player::MovementDirection::RIGHT, deltaTime, maze, blockSize);
}

std::vector<Model> generate3DMaze(Shader* shader, float& blockSize, Maze& maze)
{
	blockSize = 1.f;
	auto cubeMesh = std::make_shared<Cube>();
	std::vector<Model> blocks;

	auto addBlock = [&](const glm::vec3& worldPos) {
		blocks.emplace_back(shader, cubeMesh);
		auto& block = blocks.back();
		block.transform = glm::translate(block.transform, worldPos * blockSize);
		block.transform = glm::scale(block.transform, glm::vec3(blockSize / 2));
		block.transform = glm::rotate(block.transform, glm::radians(90.f), glm::vec3(1.f, 0.f, 0.f));
	};

	maze = MazeBuilder::generateMaze();
	for (int x = 0; x < maze.getSize(); ++x)
		for (int z = 0; z < maze.getSize(); ++z)
			if (maze[x][z] == Maze::Tile::Wall)
				for (int y = 0; y < kMazeHeight; ++y)
					addBlock({ x, y, z });


	return blocks;
}

Model createFloor(Shader* shader, unsigned int mazeSize, float blockSize)
{
	auto planeMesh = std::make_shared<Plane>();
	Model floor(shader, planeMesh);
	floor.transform = glm::translate(floor.transform, glm::vec3(mazeSize / 2 - blockSize / 2, -1.5f, mazeSize / 2 - blockSize / 2));
	floor.transform = glm::scale(floor.transform, glm::vec3(mazeSize * blockSize / 2, 1.f, mazeSize * blockSize / 2));
	floor.transform = glm::rotate(floor.transform, glm::radians(90.f), glm::vec3(1.f, 0.f, 0.f));
	return floor;
}

bool initializeGL(GLFWwindow*& window)
{
	glfwInit();
	glfwWindowHint(GLFW_SAMPLES, 8);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	window = glfwCreateWindow(kScreenWidth, kScreenHeight, "Maze3D", NULL, NULL);
	if (window == NULL)
	{
		std::cerr << "Failed to create GLFW window" << std::endl;
		glfwTerminate();
		return false;
	}
	glfwMakeContextCurrent(window);
	glfwSetFramebufferSizeCallback(window, Callbacks::resize);
	glfwSetCursorPosCallback(window, Callbacks::mouseMove);
	glfwSetScrollCallback(window, Callbacks::scroll);
	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

	glewExperimental = GL_TRUE;
	glewInit();
	glGetError();

	glViewport(0, 0, kScreenWidth, kScreenHeight);
	glClearColor(0.f, 0.f, 0.4f, 1.f);
	glEnable(GL_DEPTH_TEST);
	return true;
}


void InitAudio()
{
	audio.source->position = chestLocation;
	audio.source->SetSource();
	audio.PlayAudio();
}

glm::mat4 createChestTransform()
{
	glm::mat4 chestTransform = glm::mat4(1.0f);
	chestTransform = glm::translate(chestTransform, chestLocation);
	chestTransform = glm::scale(chestTransform, glm::vec3(0.5f));
	return chestTransform;
}

unsigned int generateDepthMap(unsigned int& depthMapFBO, unsigned int& depthMap)
{
	glGenFramebuffers(1, &depthMapFBO);
	glGenTextures(1, &depthMap);
	glBindTexture(GL_TEXTURE_2D, depthMap);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, kShadowWidth, kShadowHeight, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glBindFramebuffer(GL_FRAMEBUFFER, depthMapFBO);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depthMap, 0);
	glDrawBuffer(GL_NONE);
	glReadBuffer(GL_NONE);
	glBindFramebuffer(GL_FRAMEBUFFER, 0); 
	return depthMap;
}

#ifdef NDEBUG
#else
std::ostream& operator<<(std::ostream& outputStream, const glm::vec3& vector)
{
	outputStream << "(" << vector.x << ", " << vector.y << ", " << vector.z << ")";
	return outputStream;
}
#endif
int main()
{

	GLFWwindow* window = nullptr;

	if (!initializeGL(window))
		return -1;

	unsigned int VAO;
	glGenVertexArrays(1, &VAO);
	glBindVertexArray(VAO);

	Shader modelsShader("shaders/models.vert", "shaders/models.frag");
	Shader depthShader("shaders/depth_shader.vert", "shaders/depth_shader.frag");
	textures.emplace("bricks", Texture("textures/bricks.jpg"));
	textures.emplace("floor", Texture("textures/floor.jpg"));

	auto mazeBlocks = generate3DMaze(&modelsShader, blockSize, maze);
	auto floor = createFloor(&modelsShader, maze.getSize(), blockSize);

	auto playerStartPosition = glm::vec3(
		static_cast<float>(maze.getSize() / 2),
		static_cast<float>(kMazeHeight * blockSize / 2),
		static_cast<float>(maze.getSize() / 2)
	);

	auto playerCamera = std::make_shared<Camera>(glm::vec2(static_cast<float>(kScreenWidth), static_cast<float>(kScreenHeight)));
	player = std::make_shared<Player>(playerStartPosition, playerCamera);
	player->position = glm::vec3(maze.getSize() / 2, 1, maze.getSize() / 2);

	double deltaTime = 0.0f;
	double lastFrame = 0.0f;


	InitAudio();

	ImportedModel chestModel("models/treasure_chest.obj");
	auto chestTransform = createChestTransform();
	auto chestTilePosition = Utils::worldPositionToTilePosition(chestLocation, blockSize);


	glm::vec3 lightPos{ 16.f, 10.f, 16.f };
	unsigned int depthMap, depthMapFBO;
	generateDepthMap(depthMapFBO, depthMap);

	auto renderScene = [&](Shader& currentShader) {
		auto viewMatrix = player->camera->getViewMatrix(player->position);
		auto projMatrix = player->camera->getProjectionMatrix();
		currentShader.setMat4("view", viewMatrix);
		currentShader.setMat4("projection", projMatrix);
		currentShader.setVec3("lightPos", lightPos);
		currentShader.setVec3("viewPos", player->position);
		glBindVertexArray(VAO);
		glBindTexture(GL_TEXTURE_2D, textures["bricks"].getId());
		for (auto& block : mazeBlocks)
			block.draw();
		glBindTexture(GL_TEXTURE_2D, textures["floor"].getId());
		floor.draw();
		glBindVertexArray(0);
		currentShader.setMat4("transform", chestTransform);
		chestModel.Draw(currentShader);
	};

	modelsShader.use();
	modelsShader.setInt("texture_diffuse1", 0);
	modelsShader.setInt("shadowMap", 1);

	while (!glfwWindowShouldClose(window))
	{
		double currentFrame = glfwGetTime();
		deltaTime = currentFrame - lastFrame;
		lastFrame = currentFrame;

		processInput(window, deltaTime);
		lightPos = player->position;

		auto blockPosition = Utils::worldPositionToTilePosition(player->position, blockSize);
		#ifdef NDEBUG
		#else
		std::cout << static_cast<int>(1 / deltaTime) << " FPS | Player position: " << player->position << " | Block position: (" << blockPosition.first << ", " << blockPosition.second << ") | Block ID: " << static_cast<int>(maze[blockPosition.first][blockPosition.second]) << std::endl;
		#endif

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		depthShader.use();
		glm::mat4 lightProjection, lightView;
		glm::mat4 lightSpaceMatrix;
		float near_plane = 1.0f, far_plane = 7.5f;
		lightProjection = glm::ortho(-10.0f, 10.0f, -10.0f, 10.0f, near_plane, far_plane);
		lightView = glm::lookAt(lightPos, glm::vec3(0.0f), glm::vec3(0.0, 1.0, 0.0));
		lightSpaceMatrix = lightProjection * lightView;
		depthShader.setMat4("lightSpaceMatrix", lightSpaceMatrix);

		glViewport(0, 0, kShadowWidth, kShadowHeight);
		glBindFramebuffer(GL_FRAMEBUFFER, depthMapFBO);
		std::cerr << glGetError();
		glClear(GL_DEPTH_BUFFER_BIT);
		glActiveTexture(GL_TEXTURE0);
		renderScene(depthShader);
		glBindFramebuffer(GL_FRAMEBUFFER, 0);


		modelsShader.use();
		modelsShader.setMat4("lightSpaceMatrix", lightSpaceMatrix);
		glViewport(0, 0, player->camera->getViewport().x, player->camera->getViewport().y);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, depthMap);
		glActiveTexture(GL_TEXTURE0);
		glDisable(GL_CULL_FACE);
		renderScene(modelsShader);


		if (!gameWon)
		{
			audio.listener.position = player->position;
			audio.listener.SetListener();
			audio.listener.SetOrientation(player->camera->getUpVector(), -player->camera->getFrontVector());
		}

		if (blockPosition.first == chestTilePosition.first && blockPosition.second == chestTilePosition.second)
		{
			gameWon = true;
			audio.StopAudio();
		}

		glfwSwapBuffers(window);
		glfwPollEvents();
	}

	audio.Clean();
	glfwTerminate();
	return 0;
}