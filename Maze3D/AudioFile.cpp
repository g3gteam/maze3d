#include "AudioFile.h"

AudioFile::AudioFile()
{
	fp = NULL;
	fp = fopen("sounds/blip.wav", "rb");

	fread(type, sizeof(char), 4, fp);

	fread(&size, sizeof(DWORD), 1, fp);
	fread(type, sizeof(char), 4, fp);
	fread(type, sizeof(char), 4, fp);

	fread(&chunkSize, sizeof(DWORD), 1, fp);
	fread(&formatType, sizeof(short), 1, fp);
	fread(&channels, sizeof(short), 1, fp);
	fread(&sampleRate, sizeof(DWORD), 1, fp);
	fread(&avgBytesPerSec, sizeof(DWORD), 1, fp);
	fread(&bytesPerSample, sizeof(short), 1, fp);
	fread(&bitsPerSample, sizeof(short), 1, fp);

	fread(type, sizeof(char), 4, fp);
	fread(&dataSize, sizeof(DWORD), 1, fp);

	buffer = new unsigned char[dataSize];
	fread(buffer, sizeof(BYTE), dataSize, fp);

}


void AudioFile::CloseFile()
{
	fclose(fp);
}
