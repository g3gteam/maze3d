#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

class Camera
{
	const struct defaultValues
	{
		static constexpr float yaw = -90.0f;
		static constexpr float pitch = 0.0f;
		static constexpr float minPitch = -89.0f;
		static constexpr float maxPitch = 89.0f;
		static constexpr float sensitivity = 0.05f;
		static constexpr float fov = 60.f;
		static constexpr float minFov = 20.f;
		static constexpr float maxFov = 90.f;
		static constexpr float zNear = 0.01f;
		static constexpr float zFar = 100.f;
		static constexpr bool invertYAxis = false;
	};
public:
	Camera(const glm::vec2& viewport);
	void setViewport(const glm::vec2& viewport);
	const glm::vec2& getViewport() const;
	void processMouseMovement(const glm::vec2& position);
	void processMouseScroll(float yOffset);
	glm::mat4 getProjectionMatrix() const;
	glm::mat4 getViewMatrix(const glm::vec3& position = glm::vec3(0.f, 0.f, 0.f)) const;
	const glm::vec3& getRightVector() const;
	const glm::vec3& getUpVector() const;
	const glm::vec3& getFrontVector() const;
	float getYaw() const;

private:
	void processMouseMovementByOffset(const glm::vec2& offset);
	void updateVectors();

	glm::vec2 viewport;

	glm::vec3 front;
	glm::vec3 up;
	glm::vec3 right;
	glm::vec3 worldUp;

	float yaw = defaultValues::yaw;
	float pitch = defaultValues::pitch;

	float mouseSensitivity = defaultValues::sensitivity;
	float fov = defaultValues::fov;

	glm::vec2 lastMouse = glm::vec2(0);
	bool firstMouseMove = true;

	bool invertYAxis = defaultValues::invertYAxis;
};