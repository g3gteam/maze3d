#include "Player.h"
#include "Maze.h"
#include "Utils.h"

Player::Player(const glm::vec3 & position, std::shared_ptr<Camera> camera) :
	position(position),
	camera(camera)
{
}

void Player::processMovement(MovementDirection direction, float deltaTime, const Maze & maze, float blockSize)
{
	float velocity = (float)(speedFactor * deltaTime);
	auto yaw = camera->getYaw();
	auto& rightVector = camera->getRightVector();
	auto forwardVector = glm::vec3(cos(glm::radians(yaw)), 0, sin(glm::radians(yaw)));
	glm::vec3 newPosition;
	switch (direction)
	{
		case MovementDirection::FORWARD:
			newPosition = position + forwardVector * velocity;
			break;
		case MovementDirection::BACKWARDS:
			newPosition = position - forwardVector * velocity;
			break;
		case MovementDirection::LEFT:
			newPosition = position - rightVector * velocity;
			break;
		case MovementDirection::RIGHT:
			newPosition = position + rightVector * velocity;
			break;
	}

	auto oldTilePosition = Utils::worldPositionToTilePosition(position, blockSize);
	auto newTilePosition = Utils::worldPositionToTilePosition(newPosition, blockSize);
	bool movedTwoTiles = (abs(newTilePosition.first - oldTilePosition.first) == 1 && abs(newTilePosition.second - oldTilePosition.second) == 1);

	if (maze.isPositionAvailableForMove(Utils::worldPositionToTilePosition(newPosition, blockSize)) && !movedTwoTiles)
		position = newPosition;
}