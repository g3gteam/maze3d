#pragma once
#include <string>
#include <glm/glm.hpp>

class Utils
{
public:
	static std::string readAll(std::istream& inputStream);
	static std::pair<int, int> worldPositionToTilePosition(const glm::vec3& position, float blockSize);
};

