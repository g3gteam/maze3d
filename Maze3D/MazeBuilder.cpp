#include "MazeBuilder.h"
#include <fstream>
#include "Utils.h"

Maze MazeBuilder::generateMaze()
{
	Maze maze(33);
	std::ifstream mazeFile("mazes/1.dat");
	auto mazeString = Utils::readAll(mazeFile);
	unsigned int mazeSize = sqrt(mazeString.size());
	for (int x = 0; x < mazeSize; ++x)
		for (int z = 0; z < mazeSize; ++z)
			switch (mazeString[x * mazeSize + z])
			{
			case '0':
				maze[x][z] = Maze::Tile::None;
				break;
			case '1':
				maze[x][z] = Maze::Tile::Wall;
				break;
			default:
				maze[x][z] = Maze::Tile::None;
				break;
			}
	return maze;
}
