#pragma once

#include "AudioFile.h"
#include "AudioSource.h"
#include "Listener.h"

#include <memory>

#include "AL/efx.h"
#include "AL/alext.h"
#include "AL/efx-creative.h"
#include "AL/efx-presets.h"

class AudioMaster
{
public:
	AudioMaster();

	void SetSoundFormat();
	void SetSoundFrequency();
	void LoadAudioToBuffer();

	void PlayAudio();
	void StopAudio();

	void Clean();

public:
	AudioFile audioFile;

	ALCdevice *device;
	ALCcontext *context;

	std::shared_ptr<AudioSource> source;
	Listener listener;

	ALuint soundFormat;
	ALuint soundFrequency;
};

