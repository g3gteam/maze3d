#pragma once
#include <array>
#include <vector>
#include <GL/glew.h>
#include <glm/glm.hpp>
#include "Vertex.h"

class Shader;
class Mesh
{
public:
	using Triangle = std::array<unsigned int, 3>;

	Mesh(bool usesEBO = true);
	virtual ~Mesh();
	void addVertex(const Vertex& vertex);
	void addTriangleByIndices(const Triangle& triangle);
	void build();
	virtual void update();
	void draw();

private:
	template <typename T = GLfloat>
	static void setBufferData(GLuint bufferId, size_t size, T* data, int bufferType = GL_ARRAY_BUFFER);
	static void setVertexAttribPointer(GLuint vertexAttribArrayID, GLuint componentCount, GLuint bufferID, GLuint offset = 0);

private:
	std::vector<Vertex> vertices;
	std::vector<Triangle> indices;

	const GLfloat speed = 0.01f;
	GLuint vertexBuffer, indexBuffer;

	const bool usesEBO;
};

template <typename T>
inline static void Mesh::setBufferData(GLuint bufferId, size_t size, T* data, int bufferType)
{
	glBindBuffer(bufferType, bufferId);
	glBufferData(bufferType, size * sizeof(T), data, GL_STATIC_DRAW);
}
