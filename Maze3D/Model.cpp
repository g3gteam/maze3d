#include "Model.h"
#include "Shader.h"

Model::Model(Shader* shader, std::shared_ptr<Mesh> mesh) :
	shader(shader),
	mesh(mesh),
	transform(glm::mat4(1.0f))
{
}

void Model::draw()
{
	shader->setMat4("transform", transform);
	mesh->draw();
}
