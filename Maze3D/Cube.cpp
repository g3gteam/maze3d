#include "Cube.h"

Cube::Cube() :
	Mesh(false)
{
	addVertex({ {-1.0f, -1.0f, -1.0f}, {0.0f,  0.0f, -1.0f}, {0.0f, 0.0f} });
	addVertex({ { 1.0f,  1.0f, -1.0f}, {0.0f,  0.0f, -1.0f}, {1.0f, 1.0f} });
	addVertex({ { 1.0f, -1.0f, -1.0f}, {0.0f,  0.0f, -1.0f}, {1.0f, 0.0f} });
	addVertex({ { 1.0f,  1.0f, -1.0f}, {0.0f,  0.0f, -1.0f}, {1.0f, 1.0f} });
	addVertex({ {-1.0f, -1.0f, -1.0f}, {0.0f,  0.0f, -1.0f}, {0.0f, 0.0f} });
	addVertex({ {-1.0f,  1.0f, -1.0f}, {0.0f,  0.0f, -1.0f}, {0.0f, 1.0f} });

	addVertex({ {-1.0f, -1.0f,  1.0f}, {0.0f,  0.0f, 1.0f}, {0.0f, 0.0f} });
	addVertex({ { 1.0f, -1.0f,  1.0f}, {0.0f,  0.0f, 1.0f}, {1.0f, 0.0f} });
	addVertex({ { 1.0f,  1.0f,  1.0f}, {0.0f,  0.0f, 1.0f}, {1.0f, 1.0f} });
	addVertex({ { 1.0f,  1.0f,  1.0f}, {0.0f,  0.0f, 1.0f}, {1.0f, 1.0f} });
	addVertex({ {-1.0f,  1.0f,  1.0f}, {0.0f,  0.0f, 1.0f}, {0.0f, 1.0f} });
	addVertex({ {-1.0f, -1.0f,  1.0f}, {0.0f,  0.0f, 1.0f}, {0.0f, 0.0f} });

	addVertex({ {-1.0f,  1.0f,  1.0f}, {-1.0f,  0.0f, 0.0f}, {1.0f, 0.0f} });
	addVertex({ {-1.0f,  1.0f, -1.0f}, {-1.0f,  0.0f, 0.0f}, {1.0f, 1.0f} });
	addVertex({ {-1.0f, -1.0f, -1.0f}, {-1.0f,  0.0f, 0.0f}, {0.0f, 1.0f} });
	addVertex({ {-1.0f, -1.0f, -1.0f}, {-1.0f,  0.0f, 0.0f}, {0.0f, 1.0f} });
	addVertex({ {-1.0f, -1.0f,  1.0f}, {-1.0f,  0.0f, 0.0f}, {0.0f, 0.0f} });
	addVertex({ {-1.0f,  1.0f,  1.0f}, {-1.0f,  0.0f, 0.0f}, {1.0f, 0.0f} });

	addVertex({ { 1.0f,  1.0f,  1.0f}, {1.0f,  0.0f, 0.0f}, {1.0f, 0.0f} });
	addVertex({ { 1.0f, -1.0f, -1.0f}, {1.0f,  0.0f, 0.0f}, {0.0f, 1.0f} });
	addVertex({ { 1.0f,  1.0f, -1.0f}, {1.0f,  0.0f, 0.0f}, {1.0f, 1.0f} });
	addVertex({ { 1.0f, -1.0f, -1.0f}, {1.0f,  0.0f, 0.0f}, {0.0f, 1.0f} });
	addVertex({ { 1.0f,  1.0f,  1.0f}, {1.0f,  0.0f, 0.0f}, {1.0f, 0.0f} });
	addVertex({ { 1.0f, -1.0f,  1.0f}, {1.0f,  0.0f, 0.0f}, {0.0f, 0.0f} });

	addVertex({ {-1.0f, -1.0f, -1.0f}, {0.0f,  -1.0f, 0.0f}, {0.0f, 1.0f} });
	addVertex({ { 1.0f, -1.0f, -1.0f}, {0.0f,  -1.0f, 0.0f}, {1.0f, 1.0f} });
	addVertex({ { 1.0f, -1.0f,  1.0f}, {0.0f,  -1.0f, 0.0f}, {1.0f, 0.0f} });
	addVertex({ { 1.0f, -1.0f,  1.0f}, {0.0f,  -1.0f, 0.0f}, {1.0f, 0.0f} });
	addVertex({ {-1.0f, -1.0f,  1.0f}, {0.0f,  -1.0f, 0.0f}, {0.0f, 0.0f} });
	addVertex({ {-1.0f, -1.0f, -1.0f}, {0.0f,  -1.0f, 0.0f}, {0.0f, 1.0f} });

	addVertex({ {-1.0f,  1.0f, -1.0f}, {0.0f,  1.0f, 0.0f}, {0.0f, 1.0f} });
	addVertex({ { 1.0f,  1.0f , 1.0f}, {0.0f,  1.0f, 0.0f}, {1.0f, 0.0f} });
	addVertex({ { 1.0f,  1.0f, -1.0f}, {0.0f,  1.0f, 0.0f}, {1.0f, 1.0f} });
	addVertex({ { 1.0f,  1.0f,  1.0f}, {0.0f,  1.0f, 0.0f}, {1.0f, 0.0f} });
	addVertex({ {-1.0f,  1.0f, -1.0f}, {0.0f,  1.0f, 0.0f}, {0.0f, 1.0f} });
	addVertex({ {-1.0f,  1.0f,  1.0f}, {0.0f,  1.0f, 0.0f}, {0.0f, 0.0f} });

	build();
}
