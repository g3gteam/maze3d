#pragma once

#include <string>
#include <glm/glm.hpp>

#include <GL/glew.h>
class Shader
{
public:
	Shader(const std::string& vertexPath, const std::string& fragmentPath);
	~Shader();
	void use();

	void setBool(const std::string& name, bool value) const;
	void setFloat(const std::string& name, float value) const;
	void setInt(const std::string& name, int value) const;
	void setMat4(const std::string &name, const glm::mat4 &matrix) const;
	void setVec3(const std::string &name, const glm::vec3 &vector) const;

	unsigned int getId() const;
private:
	static bool readShader(const std::string & shaderPath, std::string& shaderString);
	bool linkShaders(unsigned int vertexShaderId, unsigned int fragmentShaderId);
	static bool compileShader(unsigned int shaderId, const char* shaderCode);
	unsigned int id;
};