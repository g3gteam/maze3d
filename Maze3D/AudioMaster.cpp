#include "AudioMaster.h"

AudioMaster::AudioMaster()
{
	device = alcOpenDevice(NULL);
	context = alcCreateContext(device, NULL);
	alcMakeContextCurrent(context);

	source = std::make_shared<AudioSource>();

	SetSoundFormat();
	SetSoundFrequency();
	LoadAudioToBuffer();

	source->SetSource();
}


void AudioMaster::SetSoundFormat()
{
	if (audioFile.bitsPerSample == 8)
	{
		if (audioFile.channels == 1)
			soundFormat = AL_FORMAT_MONO8;
		else if (audioFile.channels == 2)
			soundFormat = AL_FORMAT_STEREO8;
	}
	else if (audioFile.bitsPerSample == 16)
	{
		if (audioFile.channels == 1)
			soundFormat = AL_FORMAT_MONO16;
		else if (audioFile.channels == 2)
			soundFormat = AL_FORMAT_STEREO16;
	}
}

void AudioMaster::SetSoundFrequency()
{
	soundFrequency = audioFile.sampleRate;
}

void AudioMaster::LoadAudioToBuffer()
{
	alBufferData(source->buffer, soundFormat, audioFile.buffer, audioFile.dataSize, soundFrequency);
}

void AudioMaster::PlayAudio()
{
	alSourcePlay(source->id);
}

void AudioMaster::StopAudio()
{
	alSourceStop(source->id);
}

void AudioMaster::Clean()
{
	audioFile.CloseFile();
	delete[] audioFile.buffer;

	alDeleteSources(1, &source->id);
	alDeleteBuffers(1, &source->buffer);
	alcMakeContextCurrent(NULL);

	alcDestroyContext(context);
	alcCloseDevice(device);
}

