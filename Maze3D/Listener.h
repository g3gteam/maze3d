#pragma once

#include <iostream>
#include "AL/al.h"
#include "AL/alc.h"
#include <glm/gtc/type_ptr.hpp>

class Listener
{
public:
	Listener();
	void SetListener();
	void SetOrientation(const glm::vec3& up, const glm::vec3& front);

public:
	glm::vec3 position;
	glm::vec3 velocity;
	ALfloat orientation[6];
};

