#pragma once
#include <cstdint>
#include <utility>

class Maze
{
public:
	enum class Tile : uint8_t
	{
		None = 0,
		Wall
	};
	Maze() = default;
	Maze(uint32_t mazeSize);
	Maze(const Maze& other);
	Maze& operator=(const Maze& other);
	Tile* operator[](int x);
	uint32_t getSize() const;
	~Maze();

	bool isPositionAvailableForMove(const std::pair<int, int>& position) const;
private:
	void allocate();
	void deallocate();
	Tile** data = nullptr;
	uint32_t mazeSize = 0;
};

