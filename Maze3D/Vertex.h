#pragma once
#include <glm/glm.hpp>
#include <GL/glew.h>

class Vertex
{
public:
	Vertex(const glm::vec3& position, const glm::vec3& normal, const glm::vec2& textureCoordinates);

	glm::vec3 position;
	glm::vec3 normal;
	glm::vec2 textureCoordinates;
	GLfloat* getPositionPointer();
	GLfloat* getTextureCoordinatesPointer();
};
