#pragma once

#include <iostream>
#include "AL/al.h"
#include "AL/alc.h"
#include <glm/gtc/type_ptr.hpp>

class AudioSource
{
public:
	AudioSource();
	void SetSource();
	void SetPosition(glm::vec3 otherPosition);

public:
	ALuint id;
	ALuint buffer;

	glm::vec3 position;
	glm::vec3 velocity;
};

