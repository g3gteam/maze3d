#pragma once
#include <iostream>
#include "windows.h"
#include <cstdlib>
#include "AL/al.h"
#include "AL/alc.h"

class AudioFile
{
public:
	AudioFile();
	void CloseFile();

public:
	char type[4];
	DWORD size, chunkSize;
	short formatType, channels;
	DWORD sampleRate, avgBytesPerSec;
	short bytesPerSample, bitsPerSample;
	DWORD dataSize;

	FILE *fp;

	unsigned char* buffer;
};

