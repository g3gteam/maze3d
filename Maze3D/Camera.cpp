﻿#include "Camera.h"
#include <GL/glew.h>

Camera::Camera(const glm::vec2 & viewport) :
	up(glm::vec3(0.0f, 1.0f, 0.0f)),
	front(glm::vec3(0.0f, 0.0f, -1.0f))
{
	worldUp = up;
	setViewport(viewport);
	updateVectors();
}

void Camera::setViewport(const glm::vec2 & viewport)
{
	this->viewport = viewport;
	glViewport(0, 0, this->viewport.x, this->viewport.y);
}

const glm::vec2 & Camera::getViewport() const
{
	return viewport;
}

void Camera::processMouseMovement(const glm::vec2 & position)
{
	if (firstMouseMove) {
		lastMouse = position;
		firstMouseMove = false;
	}

	auto offset = position - lastMouse;
	offset *= mouseSensitivity;
	offset.y *= (invertYAxis ? 1 : -1);

	lastMouse = position;

	processMouseMovementByOffset(offset);
}

glm::mat4 Camera::getProjectionMatrix() const
{
	return glm::perspective(glm::radians(fov), viewport.x / viewport.y, defaultValues::zNear, defaultValues::zFar);
}

glm::mat4 Camera::getViewMatrix(const glm::vec3 & position) const
{
	return glm::lookAt(position, position + front, up);
}

const glm::vec3& Camera::getRightVector() const
{
	return right;
}

const glm::vec3 & Camera::getUpVector() const
{
	return up;
}

const glm::vec3 & Camera::getFrontVector() const
{
	return front;
}

float Camera::getYaw() const
{
	return yaw;
}

void Camera::processMouseMovementByOffset(const glm::vec2 & offset)
{
	yaw   += offset.x;
	pitch += offset.y;

	if (pitch < defaultValues::minPitch)
		pitch = defaultValues::minPitch;
	if (pitch > defaultValues::maxPitch)
		pitch = defaultValues::maxPitch;

	updateVectors();
}

void Camera::updateVectors()
{
	front.x = cos(glm::radians(yaw)) * cos(glm::radians(pitch));
	front.y = sin(glm::radians(pitch));
	front.z = sin(glm::radians(yaw)) * cos(glm::radians(pitch));
	front = glm::normalize(front);

	right = glm::normalize(glm::cross(front, worldUp));
	up = glm::normalize(glm::cross(right, front));
}

void Camera::processMouseScroll(float yOffset)
{
	if (fov >= defaultValues::minFov && fov <= defaultValues::maxFov)
		fov -= yOffset;
	if (fov > defaultValues::maxFov)
		fov = defaultValues::maxFov;
	if (fov < defaultValues::minFov)
		fov = defaultValues::minFov;
}
