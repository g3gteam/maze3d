#include "Utils.h"
#include <fstream>
#include <streambuf>

std::string Utils::readAll(std::istream & inputStream)
{
	return std::string(std::istreambuf_iterator<char>(inputStream), std::istreambuf_iterator<char>());
}

std::pair<int, int> Utils::worldPositionToTilePosition(const glm::vec3 & position, float blockSize)
{
	return { static_cast<int>(position.x + blockSize / 2), static_cast<int>(position.z + blockSize / 2) };
}