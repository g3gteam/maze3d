#include "Listener.h"

Listener::Listener():
	position(0),
	velocity(0)
{

}

void Listener::SetListener()
{
	alListenerfv(AL_POSITION, glm::value_ptr(position));
	alListenerfv(AL_VELOCITY, glm::value_ptr(velocity));
	//alListenerfv(AL_ORIENTATION, orientation);
	//alListenerf(AL_GAIN, 0.6f);
}

void Listener::SetOrientation(const glm::vec3 & up, const glm::vec3 & front)
{
	orientation[0] = up.x;
	orientation[1] = up.y;
	orientation[2] = up.z;
	orientation[3] = front.x;
	orientation[4] = front.y;
	orientation[5] = front.z;
	alListenerfv(AL_ORIENTATION, orientation);
}
