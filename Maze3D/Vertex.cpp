#include "Vertex.h"
#include <glm/gtc/type_ptr.hpp>

Vertex::Vertex(const glm::vec3& position, const glm::vec3& normal, const glm::vec2& textureCoordinates) :
	position(position), normal(normal), textureCoordinates(textureCoordinates)
{ };

GLfloat * Vertex::getPositionPointer()
{
	return glm::value_ptr(position);
}

GLfloat * Vertex::getTextureCoordinatesPointer()
{
	return glm::value_ptr(textureCoordinates);
}
