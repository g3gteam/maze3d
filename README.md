# Maze3D
Maze3D is a 3D maze exploration game writen in C++ with OpenGL.

## Requirements
To compile the project you need the followings:

- GLEW
- GLFW
- STBimage
- Assimp
- OpenAL soft